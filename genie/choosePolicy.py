import numpy as np
import random as rand

def best(genomes, fit, amount):
    """
    Get only the best elements.
    """
    if type(fit) is list:
        fit=np.array(fit)
    
    if amount>=len(fit):
        return genomes

    return [genomes[i] for i in np.argpartition(-fit,amount)[:amount]]

def semibest(genomes, fit, amount):
    """
    Return randomly with genomes with bigger fit value having bigger return probability.
    Equivalent to random.choices(genomes, weights=fit-min(fit), k=amount).
    """
    if amount>=len(fit):
        return genomes

    return rand.choices(genomes, weights=fit-min(np.min(fit)-.5,0), k=amount)

class mean_threshold:
    """
    This policy do not always return genomes amount based on amount parameter
    Returned values are determined using threshold calculated by following formula: mean(fit)+stddev(fit)*n where n is parameter.
    """
    def __init__(self, n=1, amount_usage="min"):
        """
        Create new mean_threshold policy.
        :param n: multiplier of stddev value.
        :param amount_usage: How to use amount parameter. Valid values are: "min" - amount of returned elements is always greater than this, "max" - returned amount can not be bigger than this, "none" - do not use this parameter at all.
        """
        self.n=n

        valid=['min', 'max', 'none']
        if amount_usage not in valid:
            raise ValueError('Valid values of amount_usage are '+str(valid)+', "'+amount_usage+'" given.')
        
        self.amount_usage=amount_usage

    def __call__(self, genomes, fit, amount):
        thresh=np.mean(fit)

        if self.n!=0:
            stddev=np.std(fit)
            thresh+=self.n*stddev
        
        #ret=genomes[fit>thresh]
        ret=[]
        ret_fit=[]
        for ge, fi in zip(genomes, fit):
            if fi >= thresh:
                ret.append(ge)
                ret_fit.append(fi)

        if self.amount_usage=='max':
            if len(ret)>amount:
                return best(ret, np.array(ret_fit), amount)
        else:                               #min
            if len(ret)<amount:
                return best(genomes, np.array(fit), amount)

        return ret                          #none

class mixed:
    """
    Choose policy choosing from other choosers.
    Actual chosen amount can be a bit different if repetition occurs.
    """
    def __init__(self, choosers, part, amount=None, repeat=None, max_repeat=5, overget=1.45):
        """
        Create new mixed chooser.
        :param choosers: Used choosing functions.
        :param part: Array of floating point values used to calculate amount of samples chosen from each chooser, how big is given part relative to others.
        :param amount: Maximum amount of chosen genomes from given chooser, array of those.
        :param repeat: Array of bools, if fetching from chooser can be repeated when chooser return repeating samples. Should be True unless chooser gives same results for each call(e.g. best chooser)
        :param max_repeat: Maximum amount of times the fetching is done when repetitions found. 0 for infinite repetitions.
        :param overget: Set to value bigger then one to get more elements from chooser than necessary so less repetitions is required when same genomes are returned by them.
        """
        self.choosers=choosers

        part_sum=sum(part)
        self.part=[p/part_sum for p in part]
        self.amount=amount
        self.repeat=repeat
        if repeat is not None:
            self.max_repeat=max_repeat
        else:
            self.max_repeat=1

        if overget<1:
            overget=1
        self.overget=overget

    def __call__(self, genomes, fit, amount):
        if len(genomes)<=amount:
            return genomes

        ret=[]
        ret_ids=set()               #Store id of fetched values to make sure there is no repetition
        left=amount                 #How much elements is left to get

        repetition=0
        multiplier=1
        while True:
            if left<=0 or self.max_repeat!=0 and repetition>=self.max_repeat:
                break

            to_get=[int(a*left*multiplier*self.overget) for a in self.part]

            #Get genomes an iterate over them to add them to ret
            for i,chooser,get in zip(range(len(to_get)), self.choosers,to_get):
                if repetition==0 or self.repeat[i]:
                    #print(chooser)
                    for c in chooser(genomes, fit, get):
                        #print(id(c))
                        if id(c) not in ret_ids:
                            ret_ids.add(id(c))
                            ret.append(c)

            if repetition==0:
                if self.repeat is None:
                    break

                multiplier=1/sum([p for i,p in enumerate(self.part) if self.repeat[i]])
                first_pass=False

            left=amount-len(ret)
            repetition+=1

        #Delete overflowed values from whole array
        while len(ret)>amount:                      #Overget is guilty
            overflow=len(ret)-amount

            spacing=len(ret)//(overflow+1)
            if spacing<2:                           #Somehow deletion of more than half of elements is needed
                spacing=2

            ret2=[]
            prev_index=0
            for i in range(spacing,len(ret)-spacing,spacing):
                ret2.extend(ret[prev_index:i])
                prev_index=i+1
            ret2.extend(ret[prev_index:])

            ret=ret2

        return ret
