import random as rand
import copy

class DNA:
    """
    Contain list of chromosomes and factory for element production.
    It's more instruction of how to treat every chromosome than working creature DNA.
    Working version is tuple of values used also as creature construction parameters.
    Each tuple entry has to be single chromosome, if you want more use tuple of tuples.
    """
    def __init__(self, chromosomes, factory):
        """
        Create new DNA object.
        :param chromosomes: List of chromosomes building this DNA.
        :param factory: Function taking tuple of chromosome values and returning ready creature.
        """
        self.chromosomes=chromosomes
        self.factory=factory

    def hatch(self, creature):
        """
        Change creature DNA into creature.
        :param creature: DNA of creature to create.
        :return: New instance of creature described by passed DNA value.
        """
        ret=[]

        for val, ch in zip(creature, self.chromosomes):
            ret.append(ch.hatch(val))

        return self.factory(ret)
    
    def breed(self, creatures):
        """
        Breed given creatures together
        :param creatures: Array of creatures DNA being parent of newly created creature DNA.
        :return: Child creature DNA.
        """

        ret=[]
        for i, ch in enumerate(self.chromosomes):
            ret.append(ch.breed([c[i] for c in creatures]))

        return tuple(ret)

    def mutate(self, value, environment):
        """
        Perform mutation of this DNA.
        :param value: Value of contained chromosomes.
        :param environment: Contain global parameters of mutation.
        """
        ret=[]

        mutation=environment.get_mutation_probability()
        for ch,val in zip(self.chromosomes, value):
            if rand.random()<mutation*ch.get_mutation_probability():
                ret.append(ch.mutate(val, environment))
            else:
                ret.append(val)

        return tuple(ret)

    def get_random_batch(self, amount):
        """
        Get batch of fully randomized genomes.
        :param amount: Amount of genomes to create.
        :return: List of random genomes with len equal to amount parameter.
        """
        ret=[]
        for i in range(amount):
            ge=[]
            for c in self.chromosomes:
                ge.append(c.random_value())
            ret.append(tuple(ge))
        return ret


class ChromosomeTemplate:
    def __init__(self, mutation_probability_multiplier):
        """
        Create new abstract chromosome.
        :param mutation_probability_multiplier: multiplier of mutation probability.
        """
        self.mutation_probability=mutation_probability_multiplier

    def hatch(self, value):
        """
        Change this chromosome value into creature part.
        :param value: Value of this chromosome data.
        :return: Creature part ready to use in it's constructor/factory.
        """
        raise NotImplementedError("It's abstract class")


    def breed(self, values):
        """
        Breed multiple chromosome values together. By default it's choosing randomly one of them.
        """
        return rand.choice(values)

    def mutate(self, value, environment):
        """
        Mutate chromosome with given value.
        :param value: Initial value of chromosome.
        :param environment: Rules of mutation.
        :return: New value of chromosome.
        """
        raise NotImplementedError("It's abstract class")

    def get_mutation_probability(self):
        """
        Return mutation probability multiplier.
        :return: Look above.
        """
        return self.mutation_probability

    def random_value(self):
        """
        Create randomized genome value.
        :return: Random value of this genome ready to pack into creature genome tuple/list.
        """
        raise NotImplementedError("It's abstract class")

class SubChromosome(ChromosomeTemplate):
    """
    Chromosome being actually inner DNA. Suitable for switching type of inner object of optimized Creature.
    Representation of chromosome in creature is tuple. First value is index of currently used sub DNA, second is array of tuples being different sub DNA's. That way past optimizations are non discarded during mutation.
    """
    def __init__(self, mutation_DNA_switch, DNA, override_environment=None, override_breeding=True, mutation_probability_multiplier=1):
        """
        Create new SubChromosome.
        :param mutation_DNA_switch: probability of switching sub DNA during mutation.
        :param List of DNA objects which it can choose from.
        :param override_mutation: If override environment mutation probability. Valid types are integer, float or None for no overriding. It's done by creating clone of World object and setting mutation_probability value.
            Integer: Mutation probability is set to value/sub_DNA_length so average amount mutation will be @value.
            Float:   Mutation probability is set to @value.
        :param override_breeding: If override breeding method of this chromosome. If True sub DNA chromosomes can also mix with values from other parents.
        :param mutation_probability_multiplier: multiplier of mutation probability.
        """
        super().__init__(mutation_probability_multiplier)
        
        self.DNA=DNA
        self.mutation_DNA_switch=mutation_DNA_switch

        if type(override_environment) not in [int, float, type(None)]:
            raise ValueError("Type of environment_override is %s, valid types are int, float, none."%str(type(override_environment)))

        self.override_environment=override_environment

        self.override_breeding=override_breeding

    def hatch(self, value):
        return self.DNA[value[0]].hatch(value[1][value[0]])

    def breed(self, values):
        """
        Instead of default choosing random value out of given parents it's first choosing sub dna index out of every present, after that it's breeding every parent having this sub dna active.
        It's also possible to fall back to default breeding policy. It's done using override_breeding parameter of constructor.
        """
        if self.override_breeding:
            main=rand.choice(values)                            #The most important parent giving all secondary/inactive sub DNA sequences
            index=main[0]
            
            par=[p[1][index] for p in values if p[0]==index]
            if len(par)>1:
                val=self.DNA[index].breed(par)
            else:
                val=main[1][index]

            ret=list(main[1])
            ret[index]=val
            
            return (index,tuple(ret))
        else:
            return super().breed(values)

    def mutate(self, value, environment):
        if rand.random()>self.mutation_DNA_switch and len(self.DNA)>1:
            index=value[0]
            while index==value[0]:
                index=rand.randint(0, len(self.DNA)-1)

            return (index,value[1])
        else:
            env=environment
            override_environment=self.override_environment

            if type is None:
                pass
            elif type(override_environment) is int:
                environment=copy.copy(environment)
                environment.mutation_probability=self.override_environement/len(value[1][value[0]])
            elif type(override_environment) is float:
                environment=copy.copy(environment)
                environment.mutation_probability=override_environement

            ret=list(value[1])
            ret[value[0]]=self.DNA[value[0]].mutate(value[1][value[0]],environment)

            return (value[0],ret)

    def random_value(self):
        index=rand.randint(0, len(self.DNA)-1)
        value=[d.get_random_batch(1)[0] for d in self.DNA]
        return (index, value)

class FloatChromosome(ChromosomeTemplate):
    """
    Chromosome containing only float value.
    Representation of chromosome in creature is single float.
    """
    def __init__(self, min_value, max_value, mutation_deviation=None, mutation_probability_multiplier=1):
        """
        Create new chromosome having float value.
        It's mutated by adding value from Gauss distribution.
        :param min_value: Minimal value this chromosome can have.
        :param max_value: Maximal value this chromosome can have.
        :param mutation_deviation: Standard deviation of mutation value. If not given (max-min)/2 is set.
        :param mutation_probability_multiplier: Multiplier of mutation probability.
        """
        super().__init__(mutation_probability_multiplier)

        if min_value>max_value:
            tmp=min_value
            min_value=max_value
            max_value=min_value

        self.min_value=min_value
        self.max_value=max_value

        if mutation_deviation is None:
            self.mutation_deviation=(max_value-min_value)/2
        else:
            self.mutation_deviation=mutation_deviation

    def hatch(self,value):
        return value

    def mutate(self, value, environment):
        val=self.min_value-1

        while val<self.min_value or val>self.max_value:             #Out of bounds!!
            val=value+rand.gauss(0,self.mutation_deviation)*environment.get_mutation_intensity()

        return val

    def random_value(self):
        return self.min_value+rand.random()*(self.max_value-self.min_value)

class IntChromosome(ChromosomeTemplate):
    """
    Chromosome containing only integer value.
    Representation of chromosome in creature is single integer.
    """
    def __init__(self, min_value, max_value, mutation_deviation=None, mutation_probability_multiplier=1):
        """
        Create new chromosome having int value.
        It's mutated by adding value from Gauss distribution rounded to nearest int. Rounding to 0 is exception. It's rounded to 1 or -1 to avoid mutation without change.
        :param min_value: Minimal value this chromosome can have.
        :param max_value: Maximal value this chromosome can have.
        :param mutation_deviation: Standard deviation of mutation value. If not given (max-min)/2 is set.
        :param mutation_probability_multiplier: Multiplier of mutation probability.
        """
        super().__init__(mutation_probability_multiplier)

        if min_value>max_value:
            tmp=min_value
            min_value=max_value
            max_value=min_value

        self.min_value=min_value
        self.max_value=max_value

        if mutation_deviation is None:
            self.mutation_deviation=(max_value-min_value)/2
        else:
            self.mutation_deviation=mutation_deviation

    def hatch(self,value):
        return value

    def mutate(self, value, environment):
        val=self.min_value-1

        while val<self.min_value or val>self.max_value:             #Out of bounds!!
            diff=rand.gauss(0,self.mutation_deviation)*environment.get_mutation_intensity()
            
            #Round to nearest val
            if diff>0: 
                val=int(diff+.5)
            else:
                val=int(diff-.5)
            
            #Mutation without change prevention
            if val == 0:
                val=1 if diff>0 else -1
            
            val+=value

        return val

    def random_value(self):
        return rand.randint(self.min_value, self.max_value)
