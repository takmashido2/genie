from genie import choosePolicy

class Zoo:
    """
    Class responsible for storing creatures created during evolution.
    """
    def __init__(self):
        """
        Create new zoo object.
        Because lots of parameters is needed builder approach is used. Config values are not passed to constructor, their value has to be set manually on object.
        """
        #Config, edit outside
        self.add_policy=choosePolicy.mean_threshold(amount_usage="none")
        self.add_amount=10

        self.purge_type="policy"                #What kind of purging use. Valid values are:
                                                #none: purging is not done
                                                #policy: use purge_policy
        self.purge_policy=choosePolicy.mean_threshold(amount_usage="min")       #Choose which genomes should stay
        self.purge_minimal_genomes=100                  #minimal amount of globally stored genomes

        #work parameters, do not edit outside
        self.last_epoch=0
        self.genomes=[]                                 #Array of genomes arrays for each epoch
        self.fits=[]                                    #Array of fit arrays for each epoch

    def add(self, genomes, fit, epoch=None):
        """
        Add set of genomes to zoo.
        Add by default is done to new epoch, unless epoch argument is set.
        :param genomes: List of genomes of creatures you want to add.
        :param fit: Fit value of genomes you want to add.
        :param epoch: Epoch to add creatures to. Valid types are int and None. Int indicates epoch number, None indicates new epoch.
        """
        to_add=self.add_policy([(i, genomes[i]) for i in range(len(genomes))], fit, self.add_amount)

        if len(genomes)!=len(fit):
            raise ValueError("genomes and fit arrays have to have the same length")

        if epoch is None:
            self.last_epoch+=1
            epoch=self.last_epoch

        while(len(self.genomes)<=epoch):
            self.genomes.append([])
            self.fits.append([])
        
        self.genomes[epoch].extend([a[1] for a in to_add])
        self.fits[epoch].extend([fit[a[0]] for a in to_add])

    def deduplicate(self, epoch):
        """
        Perform deduplication of entries zoo. If few same creatures found only single one is preserved
        Also deletion is done if previous epoch contain same creature. This means deduplication has to be one starting on last epoch.
        :param epoch: Epoch to do deduplication on.
        """
        new_genomes=[]
        new_fits=[]

        if epoch==0:
            previous_epoch=[]
        else:
            previous_epoch=self.genomes[epoch-1]

        for c, f in zip(self.genomes[epoch], self.fits[epoch]):
            if c not in new_genomes and c not in previous_epoch:
                new_genomes.append(c)
                new_fits.append(f)

        self.genomes[epoch]=new_genomes
        self.fits[epoch]=new_fits

    def purge(self):
        """
        Delete some obsolete creatures.
        Also performs deduplication on all epochs.
        """
        #Deduplicate
        for i in range(len(self.genomes)-1, -1, -1):
            self.deduplicate(i)

        #Purge
        if self.purge_type=="none":
            return
        elif self.purge_type=="policy":
            self.genomes, self.fits=self.apply_policy(self.purge_policy, self.purge_minimal_genomes)
        else:
            raise ValueError(self.purge_type+' purge type is invalid. Valid values are "none" and "policy"')
    
    def apply_policy(self, choose_policy, amount):
        """
        Apply given policy and return genomes and fits arrays.
        :param choose_policy: Choose policy to apply.
        :param amount: Amount of items to get.
        :return: tuple(genomes, fits) with values choosed by choose_policy.
        """
        gen=[]
        fit=[]
        
        #Concatenate all genomes into single array to be able to use choose policy
        for epoch in range(len(self.genomes)):
            for i in range(len(self.genomes[epoch])):
                gen.append((epoch,i,self.genomes[epoch][i]))
                fit.append(self.fits[epoch][i])

        gen=choose_policy(gen, fit, amount)

        new_genomes=[[] for i in range(len(self.genomes))]
        new_fits=[[] for i in range(len(self.fits))]

        for g in gen:
            new_genomes[g[0]].append(g[2])
            new_fits[g[0]].append(self.fits[g[0]][g[1]])
        
        return new_genomes, new_fits
