import random as rand
import numpy as np
import tqdm
from genie import choosePolicy
from genie.Zoo import Zoo

class World:
    """
    Class responsible for management of simulation.
    Every parameter can be either static value or function taking epoch number as argument.
    Because there is lot's of parameters the builder approach is taken, params values are not passed to constructor, their value is set directly on object.
    """
    def __init__(self, DNA, creatures, creature_simulator, zoo=None):
        """
        Create new World.
        :param DNA: DNA template of creatures living in it.
        :param creatures: List of initial creatures DNA values.
        :param environment: Configuration of environment, breeding, mutating and other parameters.
        :param creature_simulator: Function taking array of ready creatures and returning array containing every creature score.
        :param zoo: Zoo to use. If not passed new instance is created without further configuration tweaking.
        """
        #World configuration
        self.mutation_probability=.2            #It applies to every chromosome individually so with lots you may want to set it smaller
        self.mutation_intensity=1             #Multiplier used by some chromosomes to scale down mutation

        self.population_size=100                #Population size which World tries to maintain. It can be different on first iteration and when new creatures are added externally in middle of simulation.

        self.parents_group_size=30              #Number of creatures in breeding pool
        self.parents_choose_policy=choosePolicy.mixed([choosePolicy.mean_threshold(amount_usage='max'),choosePolicy.semibest],[1,1], repeat=[False, True])
        self.parents_amount=3                   #Number of creatures being parents of single creature

        self.clones_group_size=10               #Number of genomes being cloned into next epoch
        self.clones_choose_policy=choosePolicy.best
        self.clones_deduplication_period=lambda x:max(1,self.epoch_relative_max//20)      #Perform clones deduplication, sometimes identical creatures can show in pool and best clones choosing policy tend to keep them over long period of time. Delete them once per set amount of cycles to get rid of them. Negative means no deduplication.

        self.epoch_max=100                      #Simulation is force stopped after this epoch
        self.epoch_relative_max=100             #If you want to stop simulation after some epochs and do some operation(e.g. saving results) use it. It's maximum amount of epochs in single Word.simulate execution.

        self.fit_checkers=[]                    #Array of functions taking fit array as an argument and returning if stop condition occured
        self.fit_checkers_stop_number=1         #How many fit checkers has to occur to stop simulation

        if zoo is None:
            zoo=Zoo()
        self.zoo=zoo

        #Make world ready to live
        self.epoch=0

        self.DNA=DNA
        self.creatures=[]
        self.creature_simulator=creature_simulator

        self.time_clones_deduplication=self.get_clones_deduplication_period()

        self.populate(creatures)

        self.newest_added_to_zoo=False                      #Helper variable, when single simulate period ends it adds newest creatures to zoo. It's marking if that happened and skip of first loop creatures is needed.

        self.train_log={"min":[], "max":[], "avg":[], "stddev":[]}

    def populate(self,creatures):
        """
        Add new creatures DNA to the world. Note that after single simulate step at they will be dead and their place taken by children.
        :param creatures: Creatures DNA values to insert.
        """
        self.creatures.extend(creatures)

        self.newest_added_to_zoo=False

    def simulate(self):
        """
        Start whole simulation according to rules stored inside object.
        :return: Tuple with train log dictionary and Zoo object
        """

        for i in tqdm.tqdm(range(self.get_epoch_relative_max())):
            #Let the creatures reproduce
            creatures=[self.DNA.hatch(ge) for ge in self.creatures]
            fit=self.creature_simulator(creatures)

            if type(fit)==list:
                fit=np.array(fit)
            elif type(fit)!=np.ndarray:
                raise ValueError("creature simulator has to return fit value either as list/tuple of numbers or numpy array.")

            self.train_log["min"].append(np.min(fit))
            self.train_log["max"].append(np.max(fit))
            self.train_log["avg"].append(np.mean(fit))
            self.train_log["stddev"].append(np.std(fit))

            if not self.newest_added_to_zoo:
                self.zoo.add(self.creatures, fit, epoch=self.epoch)
            self.newest_added_to_zoo=False

            #print(i,":\t",min(fit),'\t',sum(fit)/len(fit),'\t',max(fit))

            clones=self.clones_choose_policy(self.creatures, fit, self.get_clones_group_size())
            parents=self.parents_choose_policy(self.creatures, fit, self.get_parents_group_size())

            if self.time_clones_deduplication>0:
                self.time_clones_deduplication-=1

                if self.time_clones_deduplication==0:
                    self.time_clones_deduplication=self.get_clones_deduplication_period()
                    cl=[]
                    for c in clones:
                        if c not in cl:
                            cl.append(c)

                    clones=cl

            childs_num=self.get_population_size()-len(clones)
            childs=[]
            if childs_num>0:
                parents_num=self.get_parents_amount()

                if parents_num>len(parents):
                    raise ValueError("Expected parents of single child number(%d) is bigger than current parents number(%d)"%(parents_num,len(parents)))

                while True:                                                     #Main children accumulating loop
                    rand.shuffle(parents)

                    for i in range(0,len(parents),parents_num):                 #Add shuffled genomes to parents pool one by one
                        if i+parents_num>len(parents):
                            continue                                            #Instead of break so loop else invoke

                        childs.append(self.DNA.breed(parents[i:i+parents_num]))

                        if len(childs)==childs_num:
                            break
                    else:
                        continue
                    break

                childs=[self.DNA.mutate(child, self) for child in childs]

            self.creatures=clones+childs

            #Check stop conditions
            if self.epoch!=None and self.epoch>self.epoch_max:
                break

            stop_points=0
            for fit_checker in self.fit_checkers:
                if fit_checker(fit):
                    stop_points+=1

            if stop_points>=self.fit_checkers_stop_number:
                break

            self.epoch+=1

        #Add newest creatures to zoo
        fit=self.creature_simulator([self.DNA.hatch(c) for c in self.creatures])

        if type(fit)==list:
            fit=np.array(fit)
        elif type(fit)!=np.ndarray:
            raise ValueError("creature simulator has to return fit value either as list/tuple of numbers or numpy array.")

        self.zoo.add(self.creatures, fit, epoch=self.epoch)
        self.newest_added_to_zoo=True
        
        self.zoo.purge()

        return self.train_log,self.zoo
    
    def get_zoo(self):
        """
        Return Zoo used by this world.
        :return: Look above.
        """
        return self.zoo

    def __get_parameter__(self, raw_value):
        """
        Decode raw parameter value and return it. Parameter may be producer of it, so it's needed.
        :return: Look above.
        """
        if callable(raw_value):
            return raw_value(self.epoch)

        return raw_value

    def get_mutation_probability(self):
        return self.__get_parameter__(self.mutation_probability)

    def get_mutation_intensity(self):
        return self.__get_parameter__(self.mutation_intensity)

    def get_population_size(self):
        return self.__get_parameter__(self.population_size)

    def get_parents_group_size(self):
        return self.__get_parameter__(self.parents_group_size)

    def get_parents_amount(self):
        return self.__get_parameter__(self.parents_amount)

    def get_clones_group_size(self):
        return self.__get_parameter__(self.clones_group_size)

    def get_clones_deduplication_period(self):
        return self.__get_parameter__(self.clones_deduplication_period)

    def get_epoch_max(self):
        return self.__get_parameter__(self.epoch_max)

    def get_epoch_relative_max(self):
        return self.__get_parameter__(self.epoch_relative_max)

class FitCheckers:
    """
    Namespace for fit checker Callables.
    """

    class FitThreshold:
        """
        Returns true if certain parameter exceeds threshold.
        """
        def __init__(self, threshold, accumulation_function):
            """
            Create new FitThreshold checker.
            :param threshold: Threshold to exceed. True will be returned when accumulated value is bigger than this.
            :param accumulation_function: Function used to accumulate fit. E.g. np.min, np.max.
            """
            self.threshold=threshold
            self.accumulator=accumulation_function

        def __call__(self, fit):
            return self.accumulator(fit)>self.threshold

    class FitAverageChangeThreshold:
        """
        Returns true if absolute value of moving average of value change is below threshold.
        """
        def __init__(self, threshold, accumulation_function, alpha=.8, warmup_epochs=10, absolute=False):
            """
            Create new FitAverageChangeThreshold checker.
            :param threshold: Threshold to exceed. True will be returned when smoothed value change is smaller than this.
            :param accumulation_function: Function used to accumulate fit. E.g. np.min, np.max.
            :param alpha: Constant from moving average formula. avg[x]=avg[x-1]*alpha+y[x]*(1-alpha)
            :param warm_up_epochs: Amount of epochs to wait for average to stabilise before checking it's value.
            :param absolute: If look at absolute value of change.
            """
            self.threshold=threshold
            self.accumulator=accumulation_function
            self.alpha=alpha
            self.warmup_epochs=warmup_epochs
            self.absolute=absolute

            self.epoch=0
            self.previous_value=None
            self.avg=None

        def __call__(self, fit):
            value=self.accumulator(fit)

            if self.previous_value is not None:
                change=value-self.previous_value
                previous_value=value

                if self.absolute:
                    change=abs(change)

                if self.avg is not None:
                    change=change*(1-self.alpha)+self.avg*self.alpha

                self.avg=change
                
                if self.epoch>=self.warmup_epochs:
                    if change<self.threshold:
                        return True
                else:
                    self.epoch+=1
            
            self.previous_value=value

            return False
