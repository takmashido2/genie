import numpy as np
from matplotlib import pyplot as plt

from genie import choosePolicy

def show_log_graph(train_log, show_stddev=True):
    """
    Create pyplot containing fit values from given train_log.
    :param train_log: train_log from World.simulate containing statistical representation of train fit values.
    :param show_stddev: If show avg+dev and avg-dev values on plot.
    """
    max_fit=np.array(train_log["max"])
    avg_fit=np.array(train_log["avg"])
    min_fit=np.array(train_log["min"])
    stddev_fit=np.array(train_log["stddev"])

    plt.plot(min_fit, label="min", color="blue")
    plt.plot(max_fit, label="max", color="red")
    plt.plot(avg_fit, label="avg", color="yellow")

    if show_stddev:
        plt.plot(avg_fit+stddev_fit, label="avg+dev", color="orange")
        plt.plot(avg_fit-stddev_fit, label="avg-dev", color="green")

    plt.legend()
    plt.show()

def print_zoo_genomes(zoo, n_best=None):
    """
    Print genomes values from Zoo object.
    :param zoo: Zoo to print.
    :param n_best: None or integer. If None all values are print, if integer only @value best results are displayed.
    """
    if n_best is None:
        genomes=zoo.genomes
        fits=zoo.fits
    elif type(n_best) is int:
        genomes, fits=zoo.apply_policy(choosePolicy.best, n_best)

    for epoch in range(len(genomes)):
        print("---------------")
        for c,f in zip(genomes[epoch], fits[epoch]):
            print(f)
            print(c)
