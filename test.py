from genie import World
from genie import misc
from genie.genome import *
from math import *
import numpy as np

def simulator(creature):
    ret=[]

    for c in creature:
        ret.append((-20/9*c[0]**2+4/3*c[0]+.2)/.4*.4
                +(-10*c[1][0]**2+18*c[1][0]-7.8)/.3*.3
                +(-10/16*c[1][1]**2+10/16*c[1][1]+23/160)/.3*.3)

    return ret

if __name__=="__main__":
    chromosomes=[]
    chromosomes.append(FloatChromosome(0, 1, .2))
    chromosomes.append(FloatChromosome(-1, 2, .2))

    dna=DNA(chromosomes, lambda x:x)

    chromosomes=[]
    chromosomes.append(FloatChromosome(0, 1, .2))
    chromosomes.append(SubChromosome(0, [dna]))

    dna=DNA(chromosomes, lambda x:x)

    #creatures=[(.5,(0,[(.5,.6,)])),(.2,(0,[(.2,.2,)]))]
    creatures=dna.get_random_batch(10)
    for i,creature in enumerate(creatures):
        c=dna.hatch(creature)
        print(c, simulator([c])[0])

    world=World.World(dna, creatures, simulator)

    world.fit_checkers.append(World.FitCheckers.FitAverageChangeThreshold(.005, np.mean))
    world.fit_checkers.append(World.FitCheckers.FitThreshold(.99, np.mean))

    #world.population_size=20
    #world.parents_group_size=6
    world.parents_amount=2
    #world.clones_group_size=4

    world.mutation_intensity=lambda x:.5/sqrt((x+1))
    world.clones_deduplication_period=1

    train_log,zoo=world.simulate()
    
    amount=0
    for epoch in range(len(zoo.genomes)):
        #print("---------------")
        amount+=len(zoo.genomes[epoch])
        for c,f in zip(zoo.genomes[epoch],zoo.fits[epoch]):
            #print(c,f)
            if f!=simulator([dna.hatch(c)]):
                print("error", c,f, simulator([dna.hatch(c)]))

    print(amount)

    misc.show_log_graph(train_log)
